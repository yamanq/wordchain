// Wordchain Solver
// Copyright (C) 2020  Yaman Qalieh

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Standard Libraries
#include <algorithm>
#include <getopt.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>

// Interfaces
#include <fstream>
#include <iostream>

// Containers
#include <list>
#include <queue>
#include <string>
#include <vector>

enum Mode {
  random_traversal, // Solve by randomly traversing the graph until quit
  max,              // Solve by traversing towards max
  brute             // Solve by traversing all possibilities
};

void print_help() {
  std::cout << "wordchain [OPTIONS]\n\n"
            << "-h: help\n"
            << "-v: Verbose mode (Default: false)\n"
            << "-l <wordlist>: Filename of newline-delimited text "
               "containing only [a-z]\n"
            << "-o <outfile>: Optionally provide a file to save the best chain so far\n"
            << "-m <mode>: Algorithm to use to create chain or 'help' (Default: random)\n"
            << "-s <seed>: Seed for random mode (Default: time)\n"
            << "-i <iterations>: Number of trials for random mode\n";
}

class Options {
public:
  std::string wordlist;
  std::string outfile;
  bool verbose = false;
  Mode mode = Mode::random_traversal;
  // Different results each time
  unsigned int seed = (unsigned int) time(nullptr);
  long int i = 1000;

  Options() {};

  Options(int argc, char *argv[]) {
    opterr = true;
    int choice;
    int option_index = 0;
    option long_options[] = {{"help", no_argument, nullptr, 'h'},
                             {"verbose", no_argument, nullptr, 'v'},
                             {"wordlist", required_argument, nullptr, 'l'},
                             {"outfile", required_argument, nullptr, 'o'},
                             {"seed", required_argument, nullptr, 's'},
                             {"mode", optional_argument, nullptr, 'm'},
                             {"iterations", required_argument, nullptr, 'i'},
                             {nullptr, 0, nullptr, '\0'}};

    while ((choice = getopt_long(argc, argv, "hvl:o:s:m:i:", long_options,
                                 &option_index)) != -1) {
      switch (choice) {
      case 'v':
        verbose = true;
        break;

      case 'l':
        wordlist = optarg;
        break;

      case 'o':
        outfile = optarg;
        break;

      case 'm':
        switch(optarg[0]) {
        case 'r':
          mode = Mode::random_traversal;
          break;

        case 'm':
          mode = Mode::max;
          break;

        case 'b':
          mode = Mode::brute;
          break;

        case 'h':
        default:
          std::cerr << "Unrecognized Mode.\n"
                    << "Valid modes are: random, max, brute\n";
          exit(1);
        }
        break;

      case 's':
        seed = std::stoi(optarg);
        break;

      case 'i':
        i = std::stol(optarg);
        break;

      case 'h':
      default:
        print_help();
        exit(1);
        break;
      }  // switch
    }    // while

    if (wordlist.empty()) {
      std::cout << "wordlist not provided.\n";
      print_help();
      exit(1);
    }
  }
};

int get_first_letter(const std::string& word) {
  return word[0] - 'a';
}

int get_last_letter(const std::string& word) {
  return word[word.length() - 1] - 'a';
}


class Words {
public:
  Words() {};
  explicit Words(Options options)
    : options(options), word_count(0) {
    words.resize(26, std::vector<std::queue<std::string>>(26));
    sizes.resize(26, 0);

    // Read words into the correct queues
    std::ifstream f(options.wordlist);
    std::string ins;
    while (std::getline(f, ins)) {
      if (options.verbose) std::cout << ins << "\n";
      words[get_first_letter(ins)][get_last_letter(ins)].push(ins);
      word_count++;
    }

    // Initial sizes vector
    for (size_t i = 0; i < words.size(); ++i) {
      sizes[i] = vsize(words[i]);
    }
  }

  void print_counts() {
    std::cout << "Counts of each letter:\n";
    for (size_t i = 0; i < 26; ++i) {
      for (size_t j = 0; j < 26; ++j) {
        std::cout << (char) ('a' + i) << "..." << (char) ('a' + j) << ": "
                  << words[i][j].size() << "\n";
      }
    }
  }

  void print_solution(std::ostream& out) {
    for (auto& word : max_list) {
      out << word << std::endl;
    }
  }

  void save_solution() {
    if (!options.outfile.empty()) {
      std::ofstream f(options.outfile);
      print_solution(f);
      f.close();
    }
  }

  void add_word(int i, int j) {
    // Exhaust all of the words that lead to the same letter
    // Unless i == j
    if (i != j) {
      sizes[i] -= words[i][i].size();
      while (words[i][i].size()) {
        max_list.push_back(words[i][i].front());
        words[i][i].pop();
      }
    }

    max_list.push_back(words[i][j].front());
    words[i][j].pop();
    --sizes[i];
  }

  bool check_chain() {
    return false;
  }

  void reset_draft_sizes() {
    for (int i = 0; i < 26; ++i) {
      for (int j = 0; j < 26; ++j) {
        draft_sizes[i][j] = words[i][j].size();
      }
    }
  }

  void max_chain_to_max_list() {
    max_list.clear();
    for (int i = 0; i < max_size; ++i) {

      max_list.push_back(words[max_chain[i]][max_chain[i + 1]].front());
      words[max_chain[i]][max_chain[i + 1]].push(max_list.back());
      words[max_chain[i]][max_chain[i + 1]].pop();
      --sizes[max_chain[i]];
    }
  }

  void solve_random_traversal() {
    solving = true;
    std::cout << "Using Seed: " << options.seed << '\n';
    srand(options.seed);

    std::vector<int> curr_chain;
    curr_chain.reserve(word_count);

    // Do i iterations of searching for a chain
    // != so that negative iteration count means non-stop
    for (long int i = 0; i != options.i; ++i) {
      if (options.verbose) std::cout << "Iteration " << i << "\n";
      reset_draft_sizes();

      // Choose first letter randomly
      curr_chain[0] = rand() % 26;

      int num_words = 0;
      // Continue until the chain ends
      do {
        cp = curr_chain[num_words];
        curr_chain[++num_words] = get_next_available(rand() % 26);
      } while (curr_chain[num_words] != -1);

      // Last 'word' is actually a fluke
      --num_words;

      if (options.verbose) {
        std::cout << "Chain:";
        for (int i = 0; i <= num_words; ++i) {
          std::cout << " " << curr_chain[i];
        }
        std::cout << std::endl;
      }

      if (num_words > max_size) {
        std::cout << "[" << i << "] New max: "
                  << num_words << " words." << std::endl;
        max_size = num_words;
        max_chain.clear();
        for (int i = 0; i <= num_words; ++i) {
          max_chain.push_back(curr_chain[i]);
        }

        max_chain_to_max_list();

        if (options.verbose) {
          print_solution(std::cout);
        }
        save_solution();
      }
    }

    solving = false;
  }

  void solve_max() {
    // Get initial max
    int maxi = -1;
    int maxj = -1;
    int maxs = 0;

    for (int i = 0; i < (int) words.size(); ++i) {
      const auto& qlist = words[i];
      for (int j = 0; j < (int) qlist.size(); ++j) {
        if ((int) qlist[j].size() > maxs && i != j) {
          maxj = j;
          maxi = i;
          maxs = (int) qlist[j].size();
        }
      }
    }

    while (maxj != -1) {
      add_word(maxi, maxj);

      maxi = get_last_letter(max_list.back());
      maxj = get_max(maxi);
    }

    save_solution();
  }

  void permute_all(int len) {

    if (len > max_size) {
      std::cout << "New Max: " << len << " words." << std::endl;
      int i = 0;

      for (auto it = curr_ilist.begin(); it != curr_ilist.end(); ++it) {
        max_chain[i++] = *it;
      }
      max_chain[i] = 0; // Null Terminator
      max_size = len;

      max_chain_to_max_list();
      if (options.verbose) print_solution(std::cout);
      save_solution();
    }

    // Try every letter
    for (int i = 0; i < 26; ++i) {
      if (draft_sizes[curr_ilist.back()][i] == 0) continue;

      --draft_sizes[curr_ilist.back()][i];
      curr_ilist.push_back(i);
      permute_all(len + 1);
      curr_ilist.pop_back();
      ++draft_sizes[curr_ilist.back()][i];
    }

  }

  // permute_all assumes at least 2 elements in curr_ilist
  void solve_brute() {
    solving = true;
    reset_draft_sizes();
    max_chain.reserve(word_count);
    for (int i = 0; i < 26; ++i) {
      curr_ilist.push_back(i);
      for (int j = 0; j < 26; ++j) {
        if (draft_sizes[i][j] == 0) continue;
        --draft_sizes[i][j];
        curr_ilist.push_back(j);
        permute_all(1);
        curr_ilist.pop_back();
        ++draft_sizes[i][j];
      }
      curr_ilist.pop_back();
    }

    solving = false;
  }

  void solve() {
    switch(options.mode) {
    case Mode::random_traversal:
      solve_random_traversal();
      break;
    case Mode::max:
      solve_max();
      break;
    case Mode::brute:
      solve_brute();
      break;
    }
  }

  // Input data
  std::vector<std::vector<std::queue<std::string>>> words;
  std::vector<size_t> sizes;
  Options options;
  size_t word_count;

  // Algorithm Data
  // Manipulate this rather than words themselves
  size_t draft_sizes[26][26];
  std::vector<int> max_chain;
  int max_size = 0;
  uint cp; // Current word prefix
  bool solving = false;

  // Used for solve_brute only
  std::list<int> curr_ilist;

  // Output data
  std::list<std::string> max_list;

  private:
  // Get next best after cp->e if that is unavailable
  int get_next_available(int e) {
    for (int i = 0; i < 26; ++i) {
      int let = (e + i) % 26;
      if (draft_sizes[cp][let] > 0) {
        draft_sizes[cp][let]--;
        return let;
      }
    }
    return -1;
  }

  // Uses sizes instead of this function
  int vsize(const std::vector<std::queue<std::string>>& qlist) {
    int sum = 0;
    for (const auto& q : qlist) {
      sum += (int) q.size();
    }
    return sum;
  }

  // Returns index of max sized queue or -1 if all 0
  int get_max(int index) {
    const std::vector<std::queue<std::string>>& qlist = words[index];
    int maxi = -1;
    size_t maxs = 0;
    for (int i = 0; i < (int) qlist.size(); ++i) {
      if (qlist[i].size() && sizes[i] > maxs) {
        maxi = i;
        maxs = qlist[i].size();
      }
    }
    return maxi;
  }
};
Words words;

void quit_handler(int s) {
  std::cout << "Caught Exit Code " << s << "\n";
  if (words.solving) {
    std::cout << "Word Chain is " << words.max_list.size() << " words long:\n";
    words.print_solution(std::cout);
    words.save_solution();
  }

  exit(s);
}

int main(int argc, char *argv[]) {
  Options options(argc, argv);

  // https://stackoverflow.com/a/1641223
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = quit_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);

  // std::ios_base::sync_with_stdio(!options.verbose);
  if (options.verbose) std::cout << "Reading wordlist from "
                                 << options.wordlist << "\n";
  words = Words(options);

  if (words.options.verbose) std::cout << "Solving wordlist\n";
  words.solve();

  std::cout << "Analysis Complete.\n"
            << "Word Chain is " << words.max_list.size() << " words long:\n";
  words.print_solution(std::cout);

  return 0;
}
