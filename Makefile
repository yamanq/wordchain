# Modified from https://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/
CXX = g++
CXXFLAGS = -std=c++17 -Wconversion -Wall -Werror -Wextra -pedantic

release: main.cpp
	$(CXX) $(CXXFLAGS) -O3 -o wordchain main.cpp

debug: main.cpp
	$(CXX) $(CXXFLAGS) -g3 -o wordchain_debug main.cpp

all: release debug

clean:
	rm -f wordchain wordchain_debug

.PHONY: release all debug clean
