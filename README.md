# Wordchain Solver
This program searches for the longest possible chain of words given a list of
input words. Each word must start with the last letter of the previous word and
must start and end with only ASCII characters. See [my blog
post](https://blog.yaman.qalieh.com/posts/wordchain/) for details.

```
-h: help
-v: Verbose mode (Default: false)
-l <wordlist>: Filename of newline-delimited text containing only [a-z]
-o <outfile>: Optionally provide a file to save the best chain so far
-m <mode>: Algorithm to use to create chain or 'help' (Default: random)
-s <seed>: Seed for random mode (Default: time)
-i <iterations>: Number of trials for random mode
```
